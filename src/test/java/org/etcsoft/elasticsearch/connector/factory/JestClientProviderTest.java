package org.etcsoft.elasticsearch.connector.factory;

import org.etcsoft.elasticsearch.connector.config.EsJestConfig;
import org.etcsoft.tools.exception.EtcsoftException;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.etcsoft.elasticsearch.connector.exception.EsErrorCodes.WRONG_ES_CONFIG;
import static org.mockito.Mockito.mock;

public class JestClientProviderTest {

	private EsJestConfig config = mock(EsJestConfig.class);

	@Test
	public void whenProviderOk_thenOk() {
		new JestClientProvider(config, true);
	}

	@Test
	public void whenProviderDefaultValues_thenOk() {
		new JestClientProvider(config, null);
	}

	@Test
	public void whenNullConfig_thenException() {
		assertThatThrownBy(() -> new JestClientProvider(null, null))
				.isInstanceOf(EtcsoftException.class)
				.hasMessage("ES config cannot be null")
				.hasFieldOrPropertyWithValue("errorCode", WRONG_ES_CONFIG)
				.hasNoCause();
	}
}
