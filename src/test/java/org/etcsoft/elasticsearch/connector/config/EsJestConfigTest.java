package org.etcsoft.elasticsearch.connector.config;

import org.etcsoft.elasticsearch.connector.config.EsJestConfig.HttpSchemaType;
import org.etcsoft.tools.exception.EtcsoftException;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.etcsoft.elasticsearch.connector.config.EsJestConfig.HttpSchemaType.HTTP;
import static org.etcsoft.elasticsearch.connector.config.EsJestConfig.HttpSchemaType.HTTPS;
import static org.etcsoft.tools.exception.UtilsErrorCode.INPUT_ERROR;

public class EsJestConfigTest {

	@Test
	public void whenEsJestConfigOK_thenOk() {
		assertWhenOk("http", "host", 9200, "user", "password", HTTP, 9200);
	}

	@Test
	public void whenHttps_thenOk() {
		assertWhenOk("https", "host", 9200, "user", "password", HTTPS, 9200);
	}

	@Test
	public void whenSchemaNull_thenOk() {
		assertWhenOk(null, "host", 9200, "user", "password", HTTP, 9200);
	}

	@Test
	public void whenPortNull_thenOk() {
		assertWhenOk("http", "host", null, "user", "password", HTTP, 9200);
	}

	@Test
	public void whenPasswordEmpty_thenOk() {
		assertWhenOk("http", "host", 9200, "user", "", HTTP, 9200);
	}

	@Test
	public void whenHostBlank_thenException() {
		assertThatThrownBy(() -> new EsJestConfig("https", "", 9200, "user", "password"))
				.isInstanceOf(EtcsoftException.class)
				.hasMessage("Host cannot be blank")
				.hasFieldOrPropertyWithValue("errorCode", INPUT_ERROR)
				.hasNoCause();

		assertThatThrownBy(() -> new EsJestConfig("https", null, 9200, "user", "password"))
				.isInstanceOf(EtcsoftException.class)
				.hasMessage("Host cannot be blank")
				.hasFieldOrPropertyWithValue("errorCode", INPUT_ERROR)
				.hasNoCause();
	}

	@Test
	public void whenUserBlank_thenException() {
		assertThatThrownBy(() -> new EsJestConfig("https", "host", 9200, "", "password"))
				.isInstanceOf(EtcsoftException.class)
				.hasMessage("User cannot be blank")
				.hasFieldOrPropertyWithValue("errorCode", INPUT_ERROR)
				.hasNoCause();

		assertThatThrownBy(() -> new EsJestConfig("https", "host", 9200, null, "password"))
				.isInstanceOf(EtcsoftException.class)
				.hasMessage("User cannot be blank")
				.hasFieldOrPropertyWithValue("errorCode", INPUT_ERROR)
				.hasNoCause();
	}

	@Test
	public void whenPasswordNull_thenException() {
		assertThatThrownBy(() -> new EsJestConfig("https", "host", 9200, "user", null))
				.isInstanceOf(EtcsoftException.class)
				.hasMessage("Password cannot be null")
				.hasFieldOrPropertyWithValue("errorCode", INPUT_ERROR)
				.hasNoCause();
	}

	private void assertWhenOk(final String httpSchema,
							  final String host,
							  final Integer port,
							  final String user,
							  final String password,
							  final HttpSchemaType httpSchemaExpected,
							  final Integer portExpected) {

		assertThat(new EsJestConfig(httpSchema, host, port, user, password))
				.hasFieldOrPropertyWithValue("httpSchema", httpSchemaExpected)
				.hasFieldOrPropertyWithValue("host", host)
				.hasFieldOrPropertyWithValue("port", portExpected)
				.hasFieldOrPropertyWithValue("user", user)
				.hasFieldOrPropertyWithValue("password", password);
	}
}
