package org.etcsoft.elasticsearch.connector.repository;

import io.searchbox.action.Action;
import io.searchbox.client.JestClient;
import io.searchbox.core.SearchResult;
import lombok.SneakyThrows;
import org.etcsoft.test.tools.assertthrown.model.ThrowableEtcsoftAssetBuilder;
import org.etcsoft.tools.exception.EtcsoftException;
import org.junit.Test;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.CompletionException;

import static org.assertj.core.api.Assertions.assertThat;
import static org.etcsoft.test.tools.assertthrown.validator.AssertThrowWrapperTestValidator.assertThrownBy;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class JestEsClientAsyncTest {

	private JestClient jestClient = mock(JestClient.class);
	private SearchResult searchResult = mock(SearchResult.class);
	private EsClient jestEsClient = new JestEsClient(jestClient);

	@Test
	@SneakyThrows
	public void whenDropIndex_thenOk() {
		when(jestClient.execute(any(Action.class))).thenReturn(searchResult);
		when(searchResult.isSucceeded()).thenReturn(true);

		assertThat(jestEsClient.dropIndexAsync("my index").join()).isEqualTo(true);
	}

	@Test
	@SneakyThrows
	public void whenCreateIndex_thenOk() {
		when(jestClient.execute(any(Action.class))).thenReturn(searchResult);
		when(searchResult.isSucceeded()).thenReturn(true);

		assertThat(jestEsClient.createIndexAsync("my index").join()).isEqualTo(true);
	}

	@Test
	@SneakyThrows
	public void whenIsIndexExists_thenOk() {
		when(jestClient.execute(any(Action.class))).thenReturn(searchResult);
		when(searchResult.isSucceeded()).thenReturn(true);

		assertThat(jestEsClient.isIndexExistsAsync("my index").join()).isEqualTo(true);
	}

	@Test
	@SneakyThrows
	public void whenIsIndexExistsFalse_thenOk() {
		when(jestClient.execute(any(Action.class))).thenReturn(searchResult);
		when(searchResult.isSucceeded()).thenReturn(false);

		assertThat(jestEsClient.isIndexExistsAsync("my index").join()).isEqualTo(false);
	}

	@Test
	@SneakyThrows
	public void whenAddAlias_thenOk() {
		when(jestClient.execute(any(Action.class))).thenReturn(searchResult);
		when(searchResult.isSucceeded()).thenReturn(true);

		assertThat(jestEsClient.addAliasAsync("my index", "my alias").join()).isEqualTo(true);
	}

	@Test
	@SneakyThrows
	public void whenPutMapping_thenOk() {
		when(jestClient.execute(any(Action.class))).thenReturn(searchResult);
		when(searchResult.isSucceeded()).thenReturn(true);

		assertThat(jestEsClient.putMappingAsync("my index", "doc type", "mapping").join()).isEqualTo(true);
	}

	@Test
	@SneakyThrows
	public void whenGetTotal_thenOk() {
		when(jestClient.execute(any(Action.class))).thenReturn(searchResult);
		when(searchResult.isSucceeded()).thenReturn(true);
		when(searchResult.getTotal()).thenReturn(55);

		assertThat(jestEsClient.getTotalAsync("my index", "doc type").join()).isEqualTo(55);
	}

	@Test
	@SneakyThrows
	public void whenGetDocs_thenOk() {
		List<PojoClassBeanTest> results = Arrays.asList(PojoClassBeanTest.builder().id(5L).name("name").build(),
														PojoClassBeanTest.builder().id(11L).name("XYZ").build());
		SearchResultTest resultTest = new SearchResultTest(true, results);

		when(jestClient.execute(any(Action.class))).thenReturn(resultTest);

		assertThat(jestEsClient.getDocsAsync("query", "my index", "doc type", PojoClassBeanTest.class).join())
				.isEqualTo(results);
	}

	@Test
	@SneakyThrows
	public void whenGetDoc_thenOk() {
		PojoClassBeanTest bean = PojoClassBeanTest.builder().id(5L).name("name").build();
		SearchResultTest resultTest = new SearchResultTest(true, Arrays.asList(bean));

		when(jestClient.execute(any(Action.class))).thenReturn(resultTest);

		assertThat(jestEsClient.getDocAsync("query", "my index", "doc type", PojoClassBeanTest.class).join())
				.isEqualTo(Optional.of(bean));
	}

	@Test
	@SneakyThrows
	public void whenInsertDoc_thenOk() {
		PojoClassBeanTest bean = PojoClassBeanTest.builder().id(5L).name("name").build();

		when(jestClient.execute(any(Action.class))).thenReturn(searchResult);
		when(searchResult.isSucceeded()).thenReturn(true);

		assertThat(jestEsClient.insertDocAsync(bean, "my index", "doc type").join()).isEqualTo(true);
	}

	@Test
	@SneakyThrows
	public void whenExecuteDsl_thenOk() {
		PojoClassBeanTest bean = PojoClassBeanTest.builder().id(5L).name("name").build();

		when(jestClient.execute(any(Action.class))).thenReturn(searchResult);
		when(searchResult.isSucceeded()).thenReturn(true);
		when(searchResult.getJsonString()).thenReturn("as String");

		assertThat(jestEsClient.executeDslAsync("query").join()).isEqualTo(Optional.of("as String"));
	}

	@Test
	@SneakyThrows
	public void whenExecuteQueryIoException_thenException() {
		when(jestClient.execute(any(Action.class))).thenThrow(new IOException("io exception"));

		ThrowableEtcsoftAssetBuilder assertion =
				ThrowableEtcsoftAssetBuilder.builder(CompletionException.class,
													 "org.etcsoft.tools.exception.EtcsoftException: Drop index 'my index'")
						.withCauseClass(EtcsoftException.class);
		assertThrownBy(() -> jestEsClient.dropIndexAsync("my index").join(), assertion);
		assertThrownBy(() -> jestEsClient.createIndexAsync("my index").join(),
					   assertion.withExactMessage("org.etcsoft.tools.exception.EtcsoftException: Create index 'my index'"));
		assertThrownBy(() -> jestEsClient.isIndexExistsAsync("my index").join(),
					   assertion.withExactMessage("org.etcsoft.tools.exception.EtcsoftException: Validate if index 'my index' exists"));
		assertThrownBy(() -> jestEsClient.isIndexExistsAsync("my index").join(),
					   assertion.withExactMessage("org.etcsoft.tools.exception.EtcsoftException: Validate if index 'my index' exists"));
		assertThrownBy(() -> jestEsClient.addAliasAsync("my index", "my alias").join(),
					   assertion.withExactMessage("org.etcsoft.tools.exception.EtcsoftException: org.etcsoft.tools.exception.EtcsoftException: Create alias 'my alias' in index 'my index'"));
		assertThrownBy(() -> jestEsClient.putMappingAsync("my index", "doc type", "mapping").join(),
					   assertion.withExactMessage("Put mapping 'mapping' in doc type 'doc type', index 'my index'"));
		assertThrownBy(() -> jestEsClient.getDocsAsync("query", "my index", "doc type", PojoClassBeanTest.class).join(),
					   assertion.withExactMessage("org.etcsoft.tools.exception.EtcsoftException: Get docs using query 'query' in index 'my index', doctype 'doc type', class type 'org.etcsoft.elasticsearch.connector.repository.PojoClassBeanTest'"));
		assertThrownBy(() -> jestEsClient.getDocAsync("query", "my index", "doc type", PojoClassBeanTest.class).join(),
					   assertion.withExactMessage("org.etcsoft.tools.exception.EtcsoftException: Get doc using this query 'query' in index 'my index', doctype 'doc type', class type 'org.etcsoft.elasticsearch.connector.repository.PojoClassBeanTest'"));
		assertThrownBy(() -> jestEsClient.insertDocAsync("String", "my index", "doc type").join(),
					   assertion.withExactMessage("org.etcsoft.tools.exception.EtcsoftException: Insert object type 'java.lang.String' in doc type 'doc type', index 'my index'"));
		assertThrownBy(() -> jestEsClient.executeDslAsync("query").join(),
					   assertion.withExactMessage("org.etcsoft.tools.exception.EtcsoftException: Get dslquery 'query'"));
		assertThrownBy(() -> jestEsClient.getTotalAsync("my index", "doc type").join(),
					   assertion
							   .addContainsMessage("Get total of docs using query ")
							   .addContainsMessage(" in index 'my index', doctype 'doc type'"));
	}

	@Test
	public void whenDropIndexWithBlankIndex_thenException() {
		ThrowableEtcsoftAssetBuilder assertion =
				ThrowableEtcsoftAssetBuilder
						.builder(CompletionException.class,
								 "org.etcsoft.tools.exception.EtcsoftException: Indexname cannot be blank")
						.withCauseClass(EtcsoftException.class);
		assertThrownBy(() -> jestEsClient.dropIndexAsync("").join(), assertion);
		assertThrownBy(() -> jestEsClient.dropIndexAsync(null).join(), assertion);
	}

	@Test
	public void whenCreateIndexWithBlankIndex_thenException() {
		ThrowableEtcsoftAssetBuilder assertion =
				ThrowableEtcsoftAssetBuilder
						.builder(CompletionException.class,
								 "org.etcsoft.tools.exception.EtcsoftException: Indexname cannot be blank")
						.withCauseClass(EtcsoftException.class);
		assertThrownBy(() -> jestEsClient.createIndexAsync("").join(), assertion);
		assertThrownBy(() -> jestEsClient.createIndexAsync(null).join(), assertion);
	}

	@Test
	public void whenIsIndexExistsWithBlankIndex_thenException() {
		ThrowableEtcsoftAssetBuilder assertion =
				ThrowableEtcsoftAssetBuilder
						.builder(CompletionException.class,
								 "org.etcsoft.tools.exception.EtcsoftException: Indexname cannot be blank")
						.withCauseClass(EtcsoftException.class);
		assertThrownBy(() -> jestEsClient.isIndexExistsAsync("").join(), assertion);
		assertThrownBy(() -> jestEsClient.isIndexExistsAsync(null).join(), assertion);
	}

	@Test
	public void whenAddAliasWithBlankIndex_thenException() {
		ThrowableEtcsoftAssetBuilder assertion =
				ThrowableEtcsoftAssetBuilder
						.builder(CompletionException.class,
								 "org.etcsoft.tools.exception.EtcsoftException: Indexname cannot be blank")
						.withCauseClass(EtcsoftException.class);
		assertThrownBy(() -> jestEsClient.addAliasAsync("", "alias").join(), assertion);
		assertThrownBy(() -> jestEsClient.addAliasAsync(null, "alias").join(), assertion);
	}

	@Test
	public void whenAddAliasWithBlankAliasName_thenException() {
		ThrowableEtcsoftAssetBuilder assertion =
				ThrowableEtcsoftAssetBuilder
						.builder(CompletionException.class,
								 "org.etcsoft.tools.exception.EtcsoftException: Alias cannot be blank")
						.withCauseClass(EtcsoftException.class);
		assertThrownBy(() -> jestEsClient.addAliasAsync("index", "").join(), assertion);
		assertThrownBy(() -> jestEsClient.addAliasAsync("index", null).join(), assertion);
	}

	@Test
	public void whenPutMappingWithBlankIndex_thenException() {
		ThrowableEtcsoftAssetBuilder assertion =
				ThrowableEtcsoftAssetBuilder
						.builder(CompletionException.class,
								 "org.etcsoft.tools.exception.EtcsoftException: Indexname cannot be blank")
						.withCauseClass(EtcsoftException.class);
		assertThrownBy(() -> jestEsClient.putMappingAsync("", "doctype", "mapping").join(), assertion);
		assertThrownBy(() -> jestEsClient.putMappingAsync(null, "doctype", "mapping").join(), assertion);
	}



//	----------

	@Test
	public void whenPutMappingWithBlankDocType_thenException() {
		ThrowableEtcsoftAssetBuilder assertion = ThrowableEtcsoftAssetBuilder.builder(CompletionException.class, 
												"org.etcsoft.tools.exception.EtcsoftException: doctype cannot be blank")
				.withCauseClass(EtcsoftException.class);
		assertThrownBy(() -> jestEsClient.putMappingAsync("index", "", "mapping").join(), assertion);
		assertThrownBy(() -> jestEsClient.putMappingAsync("index", null, "mapping").join(), assertion);
	}

	@Test
	public void whenPutMappingWithBlankMapping_thenException() {
		ThrowableEtcsoftAssetBuilder assertion = ThrowableEtcsoftAssetBuilder.builder(CompletionException.class,
												"org.etcsoft.tools.exception.EtcsoftException: mapping cannot be blank")
				.withCauseClass(EtcsoftException.class);
		assertThrownBy(() -> jestEsClient.putMappingAsync("index", "doctype", "").join(), assertion);
		assertThrownBy(() -> jestEsClient.putMappingAsync("index", "doctype", null).join(), assertion);
	}

	@Test
	public void whenGetTotalWithBlankIndex_thenException() {
		ThrowableEtcsoftAssetBuilder assertion = ThrowableEtcsoftAssetBuilder.builder(CompletionException.class,
												"org.etcsoft.tools.exception.EtcsoftException: Indexname cannot be blank").withCauseClass(EtcsoftException.class);
		assertThrownBy(() -> jestEsClient.getTotalAsync("", "doctype").join(), assertion);
		assertThrownBy(() -> jestEsClient.getTotalAsync(null, "doctype").join(), assertion);
	}

	@Test
	public void whenGetTotalWithBlankDocType_thenException() {
		ThrowableEtcsoftAssetBuilder assertion = ThrowableEtcsoftAssetBuilder.builder(CompletionException.class,
												"org.etcsoft.tools.exception.EtcsoftException: doctype cannot be blank").withCauseClass(EtcsoftException.class);
		assertThrownBy(() -> jestEsClient.getTotalAsync("index", "").join(), assertion);
		assertThrownBy(() -> jestEsClient.getTotalAsync("index", null).join(), assertion);
	}

	@Test
	public void whenGetDocsWithBlankDslQuery_thenException() {
		ThrowableEtcsoftAssetBuilder assertion =
				ThrowableEtcsoftAssetBuilder
						.builder(CompletionException.class,
								 "org.etcsoft.tools.exception.EtcsoftException: DslQuery cannot be blank")
						.withCauseClass(EtcsoftException.class);
		assertThrownBy(() -> jestEsClient.getDocsAsync("", "index", "doctype", String.class).join(), assertion);
		assertThrownBy(() -> jestEsClient.getDocsAsync(null, "index", "doctype", String.class).join(), assertion);
	}

	@Test
	public void whenGetDocsWithBlankIndex_thenException() {
		ThrowableEtcsoftAssetBuilder assertion = ThrowableEtcsoftAssetBuilder.builder(CompletionException.class,
												"org.etcsoft.tools.exception.EtcsoftException: Indexname cannot be blank").withCauseClass(EtcsoftException.class);
		assertThrownBy(() -> jestEsClient.getDocsAsync("dsl query", "", "doctype", String.class).join(), assertion);
		assertThrownBy(() -> jestEsClient.getDocsAsync("dsl query", null, "doctype", String.class).join(), assertion);
	}

	@Test
	public void whenGetDocsWithBlankDocType_thenException() {
		ThrowableEtcsoftAssetBuilder assertion = ThrowableEtcsoftAssetBuilder.builder(CompletionException.class,
												"org.etcsoft.tools.exception.EtcsoftException: doctype cannot be blank").withCauseClass(EtcsoftException.class);
		assertThrownBy(() -> jestEsClient.getDocsAsync("dsl query", "index", "", String.class).join(), assertion);
		assertThrownBy(() -> jestEsClient.getDocsAsync("dsl query", "index", null, String.class).join(), assertion);
	}

	@Test
	public void whenGetDocsWithNullClass_thenException() {
		assertThrownBy(() -> jestEsClient.getDocsAsync("dsl query", "index", "doctype", null).join(),
					   ThrowableEtcsoftAssetBuilder
							   .builder(CompletionException.class,
										"org.etcsoft.tools.exception.EtcsoftException: Object type cannot be null")
							   .withCauseClass(EtcsoftException.class));
	}

	@Test
	public void whenGetDocWithBlankDslQuery_thenException() {
		ThrowableEtcsoftAssetBuilder assertion = ThrowableEtcsoftAssetBuilder
				.builder(CompletionException.class,
						 "org.etcsoft.tools.exception.EtcsoftException: DslQuery cannot be blank")
				.withCauseClass(EtcsoftException.class);
		assertThrownBy(() -> jestEsClient.getDocAsync("", "index", "doctype", String.class).join(), assertion);
		assertThrownBy(() -> jestEsClient.getDocAsync(null, "index", "doctype", String.class).join(), assertion);
	}

	@Test
	public void whenGetDocWithBlankIndex_thenException() {
		ThrowableEtcsoftAssetBuilder assertion = ThrowableEtcsoftAssetBuilder.builder(CompletionException.class,
												"org.etcsoft.tools.exception.EtcsoftException: Indexname cannot be blank").withCauseClass(EtcsoftException.class);
		assertThrownBy(() -> jestEsClient.getDocAsync("dsl query", "", "doctype", String.class).join(), assertion);
		assertThrownBy(() -> jestEsClient.getDocAsync("dsl query", null, "doctype", String.class).join(), assertion);
	}

	@Test
	public void whenGetDocWithBlankDocType_thenException() {
		ThrowableEtcsoftAssetBuilder assertion = ThrowableEtcsoftAssetBuilder.builder(CompletionException.class,
												"org.etcsoft.tools.exception.EtcsoftException: doctype cannot be blank").withCauseClass(EtcsoftException.class);
		assertThrownBy(() -> jestEsClient.getDocAsync("dsl query", "index", "", String.class).join(), assertion);
		assertThrownBy(() -> jestEsClient.getDocAsync("dsl query", "index", null, String.class).join(), assertion);
	}

	@Test
	public void whenGetDocWithNullClass_thenException() {
		assertThrownBy(() -> jestEsClient.getDocAsync("dsl query", "index", "doctype", null).join(),
					   ThrowableEtcsoftAssetBuilder.builder(CompletionException.class,
												"org.etcsoft.tools.exception.EtcsoftException: Object type cannot be null").withCauseClass(EtcsoftException.class));
	}

	@Test
	public void whenInsertDocWithNullObject_thenException() {
		assertThrownBy(() -> jestEsClient.insertDocAsync(null, "index", "doctype").join(),
					   ThrowableEtcsoftAssetBuilder.builder(CompletionException.class,
												"org.etcsoft.tools.exception.EtcsoftException: instance cannot be null").withCauseClass(EtcsoftException.class));
	}

	@Test
	public void whenInsertDocWithBlankIndex_thenException() {
		ThrowableEtcsoftAssetBuilder assertion = ThrowableEtcsoftAssetBuilder.builder(CompletionException.class,
												"org.etcsoft.tools.exception.EtcsoftException: Indexname cannot be blank").withCauseClass(EtcsoftException.class);
		assertThrownBy(() -> jestEsClient.insertDocAsync("dsl query", "", "doctype").join(), assertion);
		assertThrownBy(() -> jestEsClient.insertDocAsync("dsl query", null, "doctype").join(), assertion);
	}

	@Test
	public void whenInsertDocWithBlankDocType_thenException() {
		ThrowableEtcsoftAssetBuilder assertion = ThrowableEtcsoftAssetBuilder.builder(CompletionException.class,
												"org.etcsoft.tools.exception.EtcsoftException: doctype cannot be blank").withCauseClass(EtcsoftException.class);
		assertThrownBy(() -> jestEsClient.insertDocAsync("dsl query", "index", "").join(), assertion);
		assertThrownBy(() -> jestEsClient.insertDocAsync("dsl query", "index", null).join(), assertion);
	}

	@Test
	public void whenExecuteDslWithBlankDslQuery_thenException() {
		ThrowableEtcsoftAssetBuilder assertion = ThrowableEtcsoftAssetBuilder.builder(CompletionException.class,
												"org.etcsoft.tools.exception.EtcsoftException: DslQuery cannot be blank").withCauseClass(EtcsoftException.class);
		assertThrownBy(() -> jestEsClient.executeDslAsync("").join(), assertion);
		assertThrownBy(() -> jestEsClient.executeDslAsync(null).join(), assertion);
	}
}
