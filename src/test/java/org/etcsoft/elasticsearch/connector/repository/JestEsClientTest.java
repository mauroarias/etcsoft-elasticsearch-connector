package org.etcsoft.elasticsearch.connector.repository;

import io.searchbox.action.Action;
import io.searchbox.client.JestClient;
import io.searchbox.core.SearchResult;
import lombok.SneakyThrows;
import org.etcsoft.test.tools.assertthrown.model.ThrowableEtcsoftAssetBuilder;
import org.etcsoft.test.tools.assertthrown.validator.AssertThrowWrapperTestValidator;
import org.junit.Test;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.etcsoft.elasticsearch.connector.exception.EsErrorCodes.ES_ACCESS;
import static org.etcsoft.tools.exception.UtilsErrorCode.INPUT_ERROR;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class JestEsClientTest {

	private JestClient jestClient = mock(JestClient.class);
	private SearchResult searchResult = mock(SearchResult.class);
	private EsClient jestEsClient = new JestEsClient(jestClient);
	private AssertThrowWrapperTestValidator assertThrownBy = new AssertThrowWrapperTestValidator();

	@Test
	@SneakyThrows
	public void whenDropIndex_thenOk() {
		when(jestClient.execute(any(Action.class))).thenReturn(searchResult);
		when(searchResult.isSucceeded()).thenReturn(true);

		assertThat(jestEsClient.dropIndex("my index")).isEqualTo(true);
	}

	@Test
	@SneakyThrows
	public void whenCreateIndex_thenOk() {
		when(jestClient.execute(any(Action.class))).thenReturn(searchResult);
		when(searchResult.isSucceeded()).thenReturn(true);

		assertThat(jestEsClient.createIndex("my index")).isEqualTo(true);
	}

	@Test
	@SneakyThrows
	public void whenIsIndexExists_thenOk() {
		when(jestClient.execute(any(Action.class))).thenReturn(searchResult);
		when(searchResult.isSucceeded()).thenReturn(true);

		assertThat(jestEsClient.isIndexExists("my index")).isEqualTo(true);
	}

	@Test
	@SneakyThrows
	public void whenIsIndexExistsFalse_thenOk() {
		when(jestClient.execute(any(Action.class))).thenReturn(searchResult);
		when(searchResult.isSucceeded()).thenReturn(false);

		assertThat(jestEsClient.isIndexExists("my index")).isEqualTo(false);
	}

	@Test
	@SneakyThrows
	public void whenAddAlias_thenOk() {
		when(jestClient.execute(any(Action.class))).thenReturn(searchResult);
		when(searchResult.isSucceeded()).thenReturn(true);

		assertThat(jestEsClient.addAlias("my index", "my alias")).isEqualTo(true);
	}

	@Test
	@SneakyThrows
	public void whenPutMapping_thenOk() {
		when(jestClient.execute(any(Action.class))).thenReturn(searchResult);
		when(searchResult.isSucceeded()).thenReturn(true);

		assertThat(jestEsClient.putMapping("my index", "doc type", "mapping")).isEqualTo(true);
	}

	@Test
	@SneakyThrows
	public void whenGetTotal_thenOk() {
		when(jestClient.execute(any(Action.class))).thenReturn(searchResult);
		when(searchResult.isSucceeded()).thenReturn(true);
		when(searchResult.getTotal()).thenReturn(55);

		assertThat(jestEsClient.getTotal("my index", "doc type")).isEqualTo(55);
	}

	@Test
	@SneakyThrows
	public void whenGetDocs_thenOk() {
		List<PojoClassBeanTest> results = Arrays.asList(PojoClassBeanTest.builder().id(5L).name("name").build(),
														PojoClassBeanTest.builder().id(11L).name("XYZ").build());
		SearchResultTest resultTest = new SearchResultTest(true, results);

		when(jestClient.execute(any(Action.class))).thenReturn(resultTest);

		assertThat(jestEsClient.getDocs("query", "my index", "doc type", PojoClassBeanTest.class))
				.isEqualTo(results);
	}

	@Test
	@SneakyThrows
	public void whenGetDoc_thenOk() {
		PojoClassBeanTest bean = PojoClassBeanTest.builder().id(5L).name("name").build();
		SearchResultTest resultTest = new SearchResultTest(true, Arrays.asList(bean));

		when(jestClient.execute(any(Action.class))).thenReturn(resultTest);

		assertThat(jestEsClient.getDoc("query", "my index", "doc type", PojoClassBeanTest.class))
				.isEqualTo(Optional.of(bean));
	}

	@Test
	@SneakyThrows
	public void whenInsertDoc_thenOk() {
		PojoClassBeanTest bean = PojoClassBeanTest.builder().id(5L).name("name").build();

		when(jestClient.execute(any(Action.class))).thenReturn(searchResult);
		when(searchResult.isSucceeded()).thenReturn(true);

		assertThat(jestEsClient.insertDoc(bean, "my index", "doc type")).isEqualTo(true);
	}

	@Test
	@SneakyThrows
	public void whenExecuteDsl_thenOk() {
		PojoClassBeanTest bean = PojoClassBeanTest.builder().id(5L).name("name").build();

		when(jestClient.execute(any(Action.class))).thenReturn(searchResult);
		when(searchResult.isSucceeded()).thenReturn(true);
		when(searchResult.getJsonString()).thenReturn("as String");

		assertThat(jestEsClient.executeDsl("query")).isEqualTo(Optional.of("as String"));
	}

	@Test
	@SneakyThrows
	public void whenExecuteQueryIoException_thenException() {
		when(jestClient.execute(any(Action.class))).thenThrow(new IOException("io exception"));

		ThrowableEtcsoftAssetBuilder assertion =
				ThrowableEtcsoftAssetBuilder
						.builder("Drop index 'my index'")
						.withErrorCode(ES_ACCESS)
						.withCauseClass(IOException.class);

		assertThrownBy.assertThrownBy(() -> jestEsClient.dropIndex("my index"), assertion);
		assertThrownBy.assertThrownBy(() -> jestEsClient.createIndex("my index"),
									  assertion.withExactMessage("Create index 'my index'"));
		assertThrownBy.assertThrownBy(() -> jestEsClient.isIndexExists("my index"),
									  assertion.withExactMessage("Validate if index 'my index' exists"));
		assertThrownBy.assertThrownBy(() -> jestEsClient.isIndexExists("my index"),
									  assertion.withExactMessage("Validate if index 'my index' exists"));
		assertThrownBy.assertThrownBy(() -> jestEsClient.addAlias("my index", "my alias"),
									  assertion.withExactMessage("Create alias 'my alias' in index 'my index'"));
		assertThrownBy.assertThrownBy(() -> jestEsClient.putMapping("my index", "doc type", "mapping"),
									  assertion.withExactMessage("Put mapping 'mapping' in doc type 'doc type', index 'my index'"));
		assertThrownBy.assertThrownBy(() -> jestEsClient.getDocs("query", "my index", "doc type", PojoClassBeanTest.class),
									  assertion.withExactMessage("Get docs using query 'query' in index 'my index', doctype 'doc type', class type 'org.etcsoft.elasticsearch.connector.repository.PojoClassBeanTest'"));
		assertThrownBy.assertThrownBy(() -> jestEsClient.getDoc("query", "my index", "doc type", PojoClassBeanTest.class),
									  assertion.withExactMessage("Get doc using this query 'query' in index 'my index', doctype 'doc type', class type 'org.etcsoft.elasticsearch.connector.repository.PojoClassBeanTest'"));
		assertThrownBy.assertThrownBy(() -> jestEsClient.insertDoc("String", "my index", "doc type"),
									  assertion.withExactMessage("Insert object type 'java.lang.String' in doc type 'doc type', index 'my index'"));
		assertThrownBy.assertThrownBy(() -> jestEsClient.executeDsl("query"), assertion.withExactMessage("Get dslquery 'query'"));
		assertThrownBy.assertThrownBy(() -> jestEsClient.getTotal("my index", "doc type"),
									  assertion
											  .addContainsMessage("Get total of docs using query ")
											  .addContainsMessage(" in index 'my index', doctype 'doc type'"));
	}

	@Test
	public void whenJestClientNull_thenException() {
		assertThrownBy.assertThrownBy(() -> new JestEsClient(null),
									  ThrowableEtcsoftAssetBuilder.builder("Jest client cannot be null").withErrorCode(INPUT_ERROR));
	}

	@Test
	public void whenDropIndexWithBlankIndex_thenException() {
		ThrowableEtcsoftAssetBuilder assertion = ThrowableEtcsoftAssetBuilder.builder("Indexname cannot be blank").withErrorCode(INPUT_ERROR);
		assertThrownBy.assertThrownBy(() -> jestEsClient.dropIndex(""), assertion);
		assertThrownBy.assertThrownBy(() -> jestEsClient.dropIndex(null), assertion);
	}

	@Test
	public void whenCreateIndexWithBlankIndex_thenException() {
		ThrowableEtcsoftAssetBuilder assertion = ThrowableEtcsoftAssetBuilder.builder("Indexname cannot be blank").withErrorCode(INPUT_ERROR);
		assertThrownBy.assertThrownBy(() -> jestEsClient.createIndex(""), assertion);
		assertThrownBy.assertThrownBy(() -> jestEsClient.createIndex(null), assertion);
	}

	@Test
	public void whenIsIndexExistsWithBlankIndex_thenException() {
		ThrowableEtcsoftAssetBuilder assertion = ThrowableEtcsoftAssetBuilder.builder("Indexname cannot be blank").withErrorCode(INPUT_ERROR);
		assertThrownBy.assertThrownBy(() -> jestEsClient.isIndexExists(""), assertion);
		assertThrownBy.assertThrownBy(() -> jestEsClient.isIndexExists(null), assertion);
	}

	@Test
	public void whenAddAliasWithBlankIndex_thenException() {
		ThrowableEtcsoftAssetBuilder assertion = ThrowableEtcsoftAssetBuilder.builder("Indexname cannot be blank").withErrorCode(INPUT_ERROR);
		assertThrownBy.assertThrownBy(() -> jestEsClient.addAlias("", "alias"), assertion);
		assertThrownBy.assertThrownBy(() -> jestEsClient.addAlias(null, "alias"), assertion);
	}

	@Test
	public void whenAddAliasWithBlankAliasName_thenException() {
		ThrowableEtcsoftAssetBuilder assertion = ThrowableEtcsoftAssetBuilder.builder("Alias cannot be blank").withErrorCode(INPUT_ERROR);
		assertThrownBy.assertThrownBy(() -> jestEsClient.addAlias("index", ""), assertion);
		assertThrownBy.assertThrownBy(() -> jestEsClient.addAlias("index", null), assertion);
	}

	@Test
	public void whenPutMappingWithBlankIndex_thenException() {
		ThrowableEtcsoftAssetBuilder assertion = ThrowableEtcsoftAssetBuilder.builder("Indexname cannot be blank").withErrorCode(INPUT_ERROR);
		assertThrownBy.assertThrownBy(() -> jestEsClient.putMapping("", "doctype", "mapping"), assertion);
		assertThrownBy.assertThrownBy(() -> jestEsClient.putMapping(null, "doctype", "mapping"), assertion);
	}

	@Test
	public void whenPutMappingWithBlankDocType_thenException() {
		ThrowableEtcsoftAssetBuilder assertion = ThrowableEtcsoftAssetBuilder.builder("doctype cannot be blank").withErrorCode(INPUT_ERROR);
		assertThrownBy.assertThrownBy(() -> jestEsClient.putMapping("index", "", "mapping"), assertion);
		assertThrownBy.assertThrownBy(() -> jestEsClient.putMapping("index", null, "mapping"), assertion);
	}

	@Test
	public void whenPutMappingWithBlankMapping_thenException() {
		ThrowableEtcsoftAssetBuilder assertion = ThrowableEtcsoftAssetBuilder.builder("mapping cannot be blank").withErrorCode(INPUT_ERROR);
		assertThrownBy.assertThrownBy(() -> jestEsClient.putMapping("index", "doctype", ""), assertion);
		assertThrownBy.assertThrownBy(() -> jestEsClient.putMapping("index", "doctype", null), assertion);
	}

	@Test
	public void whenGetTotalWithBlankIndex_thenException() {
		ThrowableEtcsoftAssetBuilder assertion = ThrowableEtcsoftAssetBuilder.builder("Indexname cannot be blank").withErrorCode(INPUT_ERROR);
		assertThrownBy.assertThrownBy(() -> jestEsClient.getTotal("", "doctype"), assertion);
		assertThrownBy.assertThrownBy(() -> jestEsClient.getTotal(null, "doctype"), assertion);
	}

	@Test
	public void whenGetTotalWithBlankDocType_thenException() {
		ThrowableEtcsoftAssetBuilder assertion = ThrowableEtcsoftAssetBuilder.builder("doctype cannot be blank").withErrorCode(INPUT_ERROR);
		assertThrownBy.assertThrownBy(() -> jestEsClient.getTotal("index", ""), assertion);
		assertThrownBy.assertThrownBy(() -> jestEsClient.getTotal("index", null), assertion);
	}

	@Test
	public void whenGetDocsWithBlankDslQuery_thenException() {
		ThrowableEtcsoftAssetBuilder assertion = ThrowableEtcsoftAssetBuilder.builder("DslQuery cannot be blank").withErrorCode(INPUT_ERROR);
		assertThrownBy.assertThrownBy(() -> jestEsClient.getDocs("", "index", "doctype", String.class), assertion);
		assertThrownBy.assertThrownBy(() -> jestEsClient.getDocs(null, "index", "doctype", String.class), assertion);
	}

	@Test
	public void whenGetDocsWithBlankIndex_thenException() {
		ThrowableEtcsoftAssetBuilder assertion = ThrowableEtcsoftAssetBuilder.builder("Indexname cannot be blank").withErrorCode(INPUT_ERROR);
		assertThrownBy.assertThrownBy(() -> jestEsClient.getDocs("dsl query", "", "doctype", String.class), assertion);
		assertThrownBy.assertThrownBy(() -> jestEsClient.getDocs("dsl query", null, "doctype", String.class), assertion);
	}

	@Test
	public void whenGetDocsWithBlankDocType_thenException() {
		ThrowableEtcsoftAssetBuilder assertion = ThrowableEtcsoftAssetBuilder.builder("doctype cannot be blank").withErrorCode(INPUT_ERROR);
		assertThrownBy.assertThrownBy(() -> jestEsClient.getDocs("dsl query", "index", "", String.class), assertion);
		assertThrownBy.assertThrownBy(() -> jestEsClient.getDocs("dsl query", "index", null, String.class), assertion);
	}

	@Test
	public void whenGetDocsWithNullClass_thenException() {
		assertThrownBy.assertThrownBy(() -> jestEsClient.getDocs("dsl query", "index", "doctype", null),
									  ThrowableEtcsoftAssetBuilder.builder("Object type cannot be null").withErrorCode(INPUT_ERROR));
	}

	@Test
	public void whenGetDocWithBlankDslQuery_thenException() {
		ThrowableEtcsoftAssetBuilder assertion = ThrowableEtcsoftAssetBuilder.builder("DslQuery cannot be blank").withErrorCode(INPUT_ERROR);
		assertThrownBy.assertThrownBy(() -> jestEsClient.getDoc("", "index", "doctype", String.class), assertion);
		assertThrownBy.assertThrownBy(() -> jestEsClient.getDoc(null, "index", "doctype", String.class), assertion);
	}

	@Test
	public void whenGetDocWithBlankIndex_thenException() {
		ThrowableEtcsoftAssetBuilder assertion = ThrowableEtcsoftAssetBuilder.builder("Indexname cannot be blank").withErrorCode(INPUT_ERROR);
		assertThrownBy.assertThrownBy(() -> jestEsClient.getDoc("dsl query", "", "doctype", String.class), assertion);
		assertThrownBy.assertThrownBy(() -> jestEsClient.getDoc("dsl query", null, "doctype", String.class), assertion);
	}

	@Test
	public void whenGetDocWithBlankDocType_thenException() {
		ThrowableEtcsoftAssetBuilder assertion = ThrowableEtcsoftAssetBuilder.builder("doctype cannot be blank").withErrorCode(INPUT_ERROR);
		assertThrownBy.assertThrownBy(() -> jestEsClient.getDoc("dsl query", "index", "", String.class), assertion);
		assertThrownBy.assertThrownBy(() -> jestEsClient.getDoc("dsl query", "index", null, String.class), assertion);
	}

	@Test
	public void whenGetDocWithNullClass_thenException() {
		assertThrownBy.assertThrownBy(() -> jestEsClient.getDoc("dsl query", "index", "doctype", null),
									  ThrowableEtcsoftAssetBuilder.builder("Object type cannot be null").withErrorCode(INPUT_ERROR));
	}

	@Test
	public void whenInsertDocWithNullObject_thenException() {
		assertThrownBy.assertThrownBy(() -> jestEsClient.insertDoc(null, "index", "doctype"),
									  ThrowableEtcsoftAssetBuilder.builder("instance cannot be null").withErrorCode(INPUT_ERROR));
	}

	@Test
	public void whenInsertDocWithBlankIndex_thenException() {
		ThrowableEtcsoftAssetBuilder assertion = ThrowableEtcsoftAssetBuilder.builder("Indexname cannot be blank").withErrorCode(INPUT_ERROR);
		assertThrownBy.assertThrownBy(() -> jestEsClient.insertDoc("dsl query", "", "doctype"), assertion);
		assertThrownBy.assertThrownBy(() -> jestEsClient.insertDoc("dsl query", null, "doctype"), assertion);
	}

	@Test
	public void whenInsertDocWithBlankDocType_thenException() {
		ThrowableEtcsoftAssetBuilder assertion = ThrowableEtcsoftAssetBuilder.builder("doctype cannot be blank").withErrorCode(INPUT_ERROR);
		assertThrownBy.assertThrownBy(() -> jestEsClient.insertDoc("dsl query", "index", ""), assertion);
		assertThrownBy.assertThrownBy(() -> jestEsClient.insertDoc("dsl query", "index", null), assertion);
	}

	@Test
	public void whenExecuteDslWithBlankDslQuery_thenException() {
		ThrowableEtcsoftAssetBuilder assertion = ThrowableEtcsoftAssetBuilder.builder("DslQuery cannot be blank").withErrorCode(INPUT_ERROR);
		assertThrownBy.assertThrownBy(() -> jestEsClient.executeDsl(""), assertion);
		assertThrownBy.assertThrownBy(() -> jestEsClient.executeDsl(null), assertion);
	}
}
