package org.etcsoft.elasticsearch.connector.repository;

import io.searchbox.core.SearchResult;
import lombok.Getter;
import org.etcsoft.tools.gson.factory.GsonFactory;

import java.util.ArrayList;
import java.util.List;

public class SearchResultTest extends SearchResult {

	@Getter
	private boolean succeeded;
	private List rawHits = new ArrayList<>();

	public SearchResultTest(boolean succeeded, List hits) {
		super(new GsonFactory().gsonFactory());
		this.succeeded = succeeded;
		this.rawHits = hits;
	}

	@Override
	public <T> List<SearchResult.Hit<T, Void>> getHits(Class<T> clazz) {
		List<SearchResult.Hit<T, Void>> hits = new ArrayList<>();
		rawHits.forEach(hit -> hits.add(new SearchResult.Hit(hit, clazz)));
		return hits;
	}

	@Override
	public <T, K> SearchResult.Hit<T, K> getFirstHit(Class<T> clazz, Class<K> explanationType) {
		Object hit = rawHits.get(0);
		return new SearchResult.Hit(hit, clazz);
	}
}
