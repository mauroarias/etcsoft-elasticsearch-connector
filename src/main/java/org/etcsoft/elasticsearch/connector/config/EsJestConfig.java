package org.etcsoft.elasticsearch.connector.config;

import lombok.Getter;
import lombok.ToString;
import org.apache.commons.lang3.StringUtils;
import org.etcsoft.tools.validation.Validator;

import java.util.Arrays;

import static org.etcsoft.elasticsearch.connector.config.EsJestConfig.HttpSchemaType.HTTP;
import static org.etcsoft.tools.exception.UtilsErrorCode.INPUT_ERROR;

@Getter
@ToString(exclude = {"password"})
public class EsJestConfig {
	private final static String BASIC_HTTP_SCHEMA = "http";
	private final static String DEFAULT_HTTP_SCHEMA = BASIC_HTTP_SCHEMA;
	private final static int DEFAULT_PORT = 9200;

	public enum HttpSchemaType {
		HTTP,
		HTTPS;

		public String getValue() {
			return this.name().toLowerCase();
		}

		public static HttpSchemaType fromValue(final String schema) {
			return HttpSchemaType.valueOf(schema.toUpperCase());
		}
	}

	private final HttpSchemaType httpSchema;
	private final String host;
	private final Integer port;
	private final String user;
	private final String password;

	private final Validator validator = new Validator();

	public EsJestConfig(final String httpSchema,
						final String host,
						final Integer port,
						final String user,
						final String password)
	{
		validator.throwIfInstanceBlank(host, "Host cannot be blank", INPUT_ERROR);
		validator.throwIfInstanceBlank(user, "User cannot be blank", INPUT_ERROR);
		validator.throwIfInstanceNull(password, "Password cannot be null", INPUT_ERROR);

		this.httpSchema = HttpSchemaType.fromValue(httpSchema == null ? DEFAULT_HTTP_SCHEMA : httpSchema);
		this.host = host;
		this.port = port == null ? DEFAULT_PORT : port;
		this.user = user;
		this.password = password;

		validator.throwIfConditionTrue(!Arrays.asList(HttpSchemaType.values()).contains(this.httpSchema),
									   "http or https are the schema supported",
									   INPUT_ERROR);
	}

	public EsJestConfig(final String host,
						final Integer port)
	{
		validator.throwIfInstanceBlank(host, "Host cannot be blank", INPUT_ERROR);

		this.httpSchema = HTTP;
		this.host = host;
		this.port = port == null ? DEFAULT_PORT : port;
		this.user = null;
		this.password = null;
	}

	public boolean isAuthenticated() {
		return StringUtils.isNotBlank(user);
	}
}
