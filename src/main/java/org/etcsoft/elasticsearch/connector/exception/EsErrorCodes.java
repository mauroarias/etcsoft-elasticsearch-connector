package org.etcsoft.elasticsearch.connector.exception;

import lombok.Getter;
import org.etcsoft.tools.exception.ErrorCode;
import org.etcsoft.tools.model.HttpStatus;

import static org.etcsoft.tools.model.HttpStatus.INTERNAL_SERVER_ERROR;

public enum EsErrorCodes implements ErrorCode {
	POJO_SERIALISING_ERROR(INTERNAL_SERVER_ERROR),
	WRONG_ES_CONFIG(INTERNAL_SERVER_ERROR),
	ES_ACCESS(INTERNAL_SERVER_ERROR);

	@Getter
	private final HttpStatus httpStatus;

	EsErrorCodes(HttpStatus httpStatus) {
		this.httpStatus = httpStatus;
	}
}
