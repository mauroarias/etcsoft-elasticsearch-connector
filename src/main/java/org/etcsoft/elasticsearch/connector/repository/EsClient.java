package org.etcsoft.elasticsearch.connector.repository;

import java.util.List;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;

public interface EsClient extends AutoCloseable
{
	Boolean dropIndex(final String indexName);
	Boolean createIndex(final String indexName);
	Boolean isIndexExists(final String indexName);
	Boolean addAlias(final String indexName, final String alias);
	Boolean removeAlias(final String indexName, final String alias);
	Boolean isAliasExists(final String alias);
	Boolean putMapping(final String indexName, final String docType, final String mappingProperties);
	Integer getTotal(final String indexName, final String docType);
	<T> List<T> getDocs(final String query, final String indexName, final String docType, final Class<T> clazz);
	<T> Optional<T> getDoc(final String query, final String indexName, final String docType, final Class<T> clazz);
	Boolean insertDoc(final Object instance, final String indexName, final String docType);
	Optional<String> executeDsl(final String dslQuery);

	CompletableFuture<Boolean> dropIndexAsync(final String indexName);
	CompletableFuture<Boolean> createIndexAsync(final String indexName);
	CompletableFuture<Boolean> isIndexExistsAsync(final String indexName);
	CompletableFuture<Boolean> addAliasAsync(final String indexName, final String alias);
	CompletableFuture<Boolean> removeAliasAsync(final String indexName, final String alias);
	CompletableFuture<Boolean> isAliasExistsAsync(final String alias);
	CompletableFuture<Integer> getTotalAsync(final String indexName, final String docType);
	CompletableFuture<Boolean> insertDocAsync(final Object instance, final String indexName, final String docType);
	CompletableFuture<Optional<String>> executeDslAsync(final String dslQuery);
	<T> CompletableFuture<List<T>> getDocsAsync(final String query,
												final String indexName,
												final String docType, Class<T> clazz);
	CompletableFuture<Boolean> putMappingAsync(final String indexName,
											   final String docType,
											   final String mappingProperties);
	<T> CompletableFuture<Optional<T>> getDocAsync(final String query,
												   final String indexName,
												   final String docType, Class<T> clazz);
}
