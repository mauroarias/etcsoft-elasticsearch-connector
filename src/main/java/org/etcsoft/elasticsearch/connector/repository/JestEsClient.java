package org.etcsoft.elasticsearch.connector.repository;

import io.searchbox.action.Action;
import io.searchbox.client.JestClient;
import io.searchbox.client.JestResult;
import io.searchbox.core.Index;
import io.searchbox.core.Search;
import io.searchbox.core.SearchResult;
import io.searchbox.indices.CreateIndex;
import io.searchbox.indices.DeleteIndex;
import io.searchbox.indices.IndicesExists;
import io.searchbox.indices.aliases.AddAliasMapping;
import io.searchbox.indices.aliases.AliasExists;
import io.searchbox.indices.aliases.ModifyAliases;
import io.searchbox.indices.aliases.RemoveAliasMapping;
import io.searchbox.indices.mapping.DeleteMapping;
import io.searchbox.indices.mapping.PutMapping;
import lombok.Getter;
import org.etcsoft.tools.exception.EtcsoftException;
import org.etcsoft.tools.validation.Validator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PreDestroy;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

import static java.lang.String.format;
import static java.util.Optional.empty;
import static java.util.Optional.of;
import static org.etcsoft.elasticsearch.connector.exception.EsErrorCodes.ES_ACCESS;
import static org.etcsoft.tools.exception.UtilsErrorCode.INPUT_ERROR;
import static org.etcsoft.tools.exception.UtilsErrorCode.UNKNOWN_ERROR;

public final class JestEsClient implements EsClient {
	private final static Logger logger = LoggerFactory.getLogger(JestEsClient.class);

	@Getter
	private final JestClient client;
	private final Validator validator = new Validator();

	public JestEsClient(final JestClient client) {
		validator.throwIfInstanceNull(client, "Jest client cannot be null", INPUT_ERROR);

		this.client = client;
	}

	@Override
	public Boolean dropIndex(final String indexName) {
		validator.throwIfInstanceBlank(indexName, "Indexname cannot be blank", INPUT_ERROR);

		return cmdExecutor(new DeleteIndex.Builder(indexName).build(), format("Drop index '%s'", indexName))
				.isSucceeded();
	}

	@Override
	public Boolean createIndex(final String indexName) {
		validator.throwIfInstanceBlank(indexName, "Indexname cannot be blank", INPUT_ERROR);

		return cmdExecutor(new CreateIndex.Builder(indexName).build(),
						   format("Create index '%s'", indexName)).isSucceeded();
	}

	@Override
	public Boolean isIndexExists(final String indexName) {
		validator.throwIfInstanceBlank(indexName, "Indexname cannot be blank", INPUT_ERROR);

		return cmdExecutor(new IndicesExists.Builder(indexName).build(),
						   format("Validate if index '%s' exists", indexName)).isSucceeded();
	}

	@Override
	public Boolean addAlias(final String indexName, final String alias) {
		validator.throwIfInstanceBlank(indexName, "Indexname cannot be blank", INPUT_ERROR);
		validator.throwIfInstanceBlank(alias, "Alias cannot be blank", INPUT_ERROR);

		return cmdExecutor(new ModifyAliases.Builder(new AddAliasMapping.Builder(indexName, alias).build()).build(),
						   format("Create alias '%s' in index '%s'", alias, indexName)).isSucceeded();
	}

	@Override
	public Boolean removeAlias(final String indexName, final String alias) {
		validator.throwIfInstanceBlank(indexName, "Indexname cannot be blank", INPUT_ERROR);
		validator.throwIfInstanceBlank(alias, "Alias cannot be blank", INPUT_ERROR);

		return cmdExecutor(new ModifyAliases.Builder(new RemoveAliasMapping.Builder(indexName, alias).build()).build(),
						   format("Removing alias '%s' in index '%s'", alias, indexName)).isSucceeded();
	}

	@Override
	public Boolean isAliasExists(final String alias) {
		validator.throwIfInstanceBlank(alias, "Alias cannot be blank", INPUT_ERROR);

		return cmdExecutor(new AliasExists.Builder().alias(alias).build(),
						   format("Validate if alias '%s' exists", alias)).isSucceeded();
	}

	@Override
	public Boolean putMapping(final String indexName, final String docType, final String mappingProperties) {
		validator.throwIfInstanceBlank(indexName, "Indexname cannot be blank", INPUT_ERROR);
		validator.throwIfInstanceBlank(docType, "doctype cannot be blank", INPUT_ERROR);
		validator.throwIfInstanceBlank(mappingProperties, "mapping cannot be blank", INPUT_ERROR);

		return cmdExecutor(new PutMapping
								   .Builder(indexName, docType, format("{ \"%s\": %s }", docType, mappingProperties))
								   .build(),
						   format("Put mapping '%s' in doc type '%s', index '%s'",
								  mappingProperties,
								  docType,
								  indexName)).isSucceeded();
	}

	@Override
	public Boolean deleteMapping(final String indexName, final String docType) {
		validator.throwIfInstanceBlank(indexName, "Indexname cannot be blank", INPUT_ERROR);
		validator.throwIfInstanceBlank(docType, "doctype cannot be blank", INPUT_ERROR);

		return cmdExecutor(new DeleteMapping
								   .Builder(indexName, docType)
								   .build(),
						   format("Deleting mapping in doc type '%s', index '%s'",
								  docType,
								  indexName)).isSucceeded();
	}

	@Override
	public Integer getTotal(final String indexName, final String docType) {
		validator.throwIfInstanceBlank(indexName, "Indexname cannot be blank", INPUT_ERROR);
		validator.throwIfInstanceBlank(docType, "doctype cannot be blank", INPUT_ERROR);

		final String getAllCounter = "{\n" +
									 "    \"size\":0," +
									 "    \"query\" : {\n" +
									 "        \"match_all\" : {}\n" +
									 "    }\n" +
									 "}";
		return executor(new Search.Builder(getAllCounter).addIndex(indexName).addType(docType).build(),
						format("Get total of docs using query '%s' in index '%s', doctype '%s'",
							   getAllCounter,
							   indexName,
							   docType))
				.map(SearchResult::getTotal)
				.orElse(0);
	}

	@Override
	public <T> List<T> getDocs(final String dslQuery, 
							   final String indexName, 
							   final String docType, 
							   final Class<T> clazz) {
		validator.throwIfInstanceBlank(dslQuery, "DslQuery cannot be blank", INPUT_ERROR);
		validator.throwIfInstanceBlank(indexName, "Indexname cannot be blank", INPUT_ERROR);
		validator.throwIfInstanceBlank(docType, "doctype cannot be blank", INPUT_ERROR);
		validator.throwIfInstanceNull(clazz, "Object type cannot be null", INPUT_ERROR);

		List<T> docs = new ArrayList<>();
		executor(new Search.Builder(dslQuery).addIndex(indexName).addType(docType).build(),
				 format("Get docs using query '%s' in index '%s', doctype '%s', class type '%s'",
						dslQuery,
						indexName,
						docType,
						clazz.getTypeName()))
				.ifPresent(result -> {
					if(result.getHits(clazz) != null) {
						docs.addAll(result.getHits(clazz).stream().map(hit -> hit.source).collect(Collectors.toList()));
					}
				});
		return docs;
	}

	@Override
	public <T> Optional<T> getDoc(final String dslQuery, 
								  final String indexName, 
								  final String docType, 
								  final Class<T> clazz) {
		validator.throwIfInstanceBlank(dslQuery, "DslQuery cannot be blank", INPUT_ERROR);
		validator.throwIfInstanceBlank(indexName, "Indexname cannot be blank", INPUT_ERROR);
		validator.throwIfInstanceBlank(docType, "doctype cannot be blank", INPUT_ERROR);
		validator.throwIfInstanceNull(clazz, "Object type cannot be null", INPUT_ERROR);

		return executor(new Search.Builder(dslQuery).addIndex(indexName).addType(docType).build(),
						format("Get doc using this query '%s' in index '%s', doctype '%s', class type '%s'",
							   dslQuery,
							   indexName,
							   docType,
							   clazz.getTypeName()))
				.map(result -> result.getFirstHit(clazz))
				.map(result -> result.source);
	}

	@Override
	public Boolean insertDoc(final Object instance, final String indexName, final String docType) {
		validator.throwIfInstanceBlank(indexName, "Indexname cannot be blank", INPUT_ERROR);
		validator.throwIfInstanceBlank(docType, "doctype cannot be blank", INPUT_ERROR);
		validator.throwIfInstanceNull(instance, "instance cannot be null", INPUT_ERROR);

		return executor(new Index.Builder(instance).index(indexName).type(docType).build(),
						format("Insert object type '%s' in doc type '%s', index '%s'",
							   instance.getClass().getTypeName(),
							   docType,
							   indexName))
				.map(JestResult::isSucceeded)
				.orElse(false);
	}

	@Override
	public Optional<String> executeDsl(final String dslQuery) {
		validator.throwIfInstanceBlank(dslQuery, "DslQuery cannot be blank", INPUT_ERROR);

		return executor(new Search.Builder(dslQuery).build(), format("Get dslquery '%s'", dslQuery))
				.map(SearchResult::getJsonString);
	}

	@Override
	public CompletableFuture<Boolean> dropIndexAsync(final String indexName) {
		return CompletableFuture.supplyAsync(() -> dropIndex(indexName));
	}

	@Override
	public CompletableFuture<Boolean> createIndexAsync(final String indexName) {
		return CompletableFuture.supplyAsync(() -> createIndex(indexName));
	}

	@Override
	public CompletableFuture<Boolean> isIndexExistsAsync(final String indexName) {
		return CompletableFuture.supplyAsync(() -> isIndexExists(indexName));
	}

	@Override
	public CompletableFuture<Boolean> addAliasAsync(final String indexName, final String alias) {
		return CompletableFuture.supplyAsync(() -> addAlias(indexName, alias));
	}

	@Override
	public CompletableFuture<Boolean> removeAliasAsync(final String indexName, final String alias) {
		return CompletableFuture.supplyAsync(() -> removeAlias(indexName, alias));
	}

	@Override
	public CompletableFuture<Boolean> isAliasExistsAsync(final String alias) {
		return CompletableFuture.supplyAsync(() -> isAliasExists(alias));
	}

	@Override
	public CompletableFuture<Boolean> putMappingAsync(final String indexName,
													  final String docType,
													  final String mappingProperties) {
		return CompletableFuture.supplyAsync(() -> putMapping(indexName, docType, mappingProperties));
	}

	@Override
	public CompletableFuture<Integer> getTotalAsync(final String indexName, final String docType) {
		return CompletableFuture.supplyAsync(() -> getTotal(indexName, docType));
	}

	@Override
	public <T> CompletableFuture<List<T>> getDocsAsync(final String query,
													   final String indexName,
													   final String docType,
													   final Class<T> clazz) {
		return CompletableFuture.supplyAsync(() -> getDocs(query, indexName, docType, clazz));
	}

	@Override
	public <T> CompletableFuture<Optional<T>> getDocAsync(final String query,
														  final String indexName,
														  final String docType,
														  final Class<T> clazz) {
		return CompletableFuture.supplyAsync(() -> getDoc(query, indexName, docType, clazz));
	}

	@Override
	public CompletableFuture<Boolean> insertDocAsync(final Object instance,
													 final String indexName,
													 final String docType) {
		return CompletableFuture.supplyAsync(() -> insertDoc(instance, indexName, docType));
	}

	@Override
	public CompletableFuture<Optional<String>> executeDslAsync(final String dslQuery) {
		return CompletableFuture.supplyAsync(() -> executeDsl(dslQuery));
	}

	@Override
	@PreDestroy
	public void close() throws Exception {
		getClient().shutdownClient();
	}

	private Optional<SearchResult> executor(final Action<?> action, final String message) {
		final SearchResult result = (SearchResult) cmdExecutor(action, message);
		if(result.isSucceeded()) {
			return of(result);
		}
		else {
			logger.error("Impossible executeDsl action '{}'", message);
		}
		return empty();
	}

	private JestResult cmdExecutor(final Action<?> action, final String message) {
		try {
			return getClient().execute(action);
		}
		catch(EtcsoftException ex) {
			throw ex;
		}
		catch(IOException cause) {
			throw new EtcsoftException(ES_ACCESS, message, cause);
		}
		catch(Exception cause) {
			throw new EtcsoftException(UNKNOWN_ERROR, message, cause);
		}
	}
}