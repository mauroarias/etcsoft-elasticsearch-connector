package org.etcsoft.elasticsearch.connector.factory;

import com.google.gson.Gson;
import io.searchbox.client.JestClientFactory;
import io.searchbox.client.config.HttpClientConfig;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustStrategy;
import org.apache.http.nio.conn.SchemeIOSessionStrategy;
import org.apache.http.nio.conn.ssl.SSLIOSessionStrategy;
import org.apache.http.ssl.SSLContextBuilder;
import org.etcsoft.elasticsearch.connector.config.EsJestConfig;
import org.etcsoft.elasticsearch.connector.exception.EsErrorCodes;
import org.etcsoft.elasticsearch.connector.repository.EsClient;
import org.etcsoft.elasticsearch.connector.repository.JestEsClient;
import org.etcsoft.tools.exception.EtcsoftException;
import org.etcsoft.tools.gson.factory.GsonFactory;
import org.etcsoft.tools.validation.Validator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;

import static java.lang.String.format;
import static org.etcsoft.elasticsearch.connector.config.EsJestConfig.HttpSchemaType.HTTP;
import static org.etcsoft.elasticsearch.connector.exception.EsErrorCodes.WRONG_ES_CONFIG;

public class JestClientProvider {

	private final static boolean DEFAULT_MULTITHREADING = true;

	private final static Logger logger = LoggerFactory.getLogger(JestClientProvider.class);
	private final Validator validation = new Validator();

	private final EsJestConfig esJestConfig;
	private final boolean multiThreading;

	public JestClientProvider(final EsJestConfig esJestConfig,
							  final Boolean multiThreading) {
		validation.throwIfInstanceNull(esJestConfig, "ES config cannot be null", WRONG_ES_CONFIG);

		this.esJestConfig = esJestConfig;
		this.multiThreading = multiThreading == null ? DEFAULT_MULTITHREADING : multiThreading;
	}


	public EsClient getClient() {
		try {
			final String uri = new URIBuilder()
					.setHost(esJestConfig.getHost())
					.setPort(esJestConfig.getPort())
					.setScheme(esJestConfig.getHttpSchema().getValue())
					.build()
					.toString();

			final Gson gson = new GsonFactory().gsonFactory();
			final HttpClientConfig.Builder clientConfigBuilder =
					new HttpClientConfig.Builder(uri).multiThreaded(multiThreading).gson(gson);
			if(esJestConfig.isAuthenticated()) {
				clientConfigBuilder.defaultCredentials(esJestConfig.getUser(), esJestConfig.getPassword());
			}

			if(esJestConfig.getHttpSchema() == HTTP) {
				final SSLContext sslContext =
						new SSLContextBuilder().loadTrustMaterial(null, (TrustStrategy) (arg0, arg1) -> true).build();

				final HostnameVerifier hostnameVerifier = NoopHostnameVerifier.INSTANCE;
				final SSLConnectionSocketFactory sslSocketFactory = new SSLConnectionSocketFactory(sslContext, hostnameVerifier);
				final SchemeIOSessionStrategy httpsIOSessionStrategy = new SSLIOSessionStrategy(sslContext, hostnameVerifier);
				clientConfigBuilder
						.defaultSchemeForDiscoveredNodes(esJestConfig.getHttpSchema().getValue())
						.sslSocketFactory(sslSocketFactory)
						.httpsIOSessionStrategy(httpsIOSessionStrategy);
			}

			final HttpClientConfig clientConfig = clientConfigBuilder.build();
			final JestClientFactory factory = new JestClientFactory();
			factory.setHttpClientConfig(clientConfig);
			return new JestEsClient(factory.getObject());
		}
		catch(Exception cause) {
			throw new EtcsoftException(EsErrorCodes.ES_ACCESS,
									   format("Error connecting to es using jest client, config: %s",
											  esJestConfig.toString(),
											  cause.getMessage()),
									   cause);
		}
	}
}
